package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"os/user"
	"strconv"
	"syscall"
	"time"
)

func notify(cmd []string) {
	cmdGoVer := &exec.Cmd{
		Path:   cmd[0],
		Args:   cmd,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}
	if err := cmdGoVer.Run(); err != nil {
		fmt.Println("Error:", err)
	}
}

func main() {
	var t int
	flag.IntVar(&t, "t", 1, "Time in Hours")
	flag.Parse()
	t = t * 2
	min := t * 30
	var count25 int = 1
	var count4 int = 1
	var flag5 int = 0
	var flag15 int = 0
	var i int = 1
	c, _ := exec.LookPath("pomodoro-notify")
	var fi *os.File

	user, err := user.Current()
	if err != nil {
		fmt.Println("Error:", err)
	}
	filePath := user.HomeDir + "/.pomodoro-gooutput"
	fmt.Println(filePath)
	os.Remove(filePath)
	fi, err = os.Create(filePath)
	if err != nil {
		fmt.Println("\nError:", err)
	}

	systemSignals := make(chan os.Signal)
	signal.Notify(systemSignals, os.Interrupt, syscall.SIGTERM)
	signal.Notify(systemSignals, os.Interrupt, syscall.SIGKILL)
	go func() {
		<-systemSignals
		err = os.Truncate(filePath, 0)
		//fi.WriteString(" ")
		os.Exit(1)
	}()
	defer fi.Close()

	_, err = fi.WriteString("\n 祥 " + strconv.Itoa(min))
	if err != nil {
		fmt.Println("\nError:", err)
	}
	for i <= t*30 {
		if count25 <= 25 {
			time.Sleep(1 * time.Minute)
			if err != nil {
				fmt.Println("Error:", err)
			}
			count25++
			if count25 <= 25 {
				_, err = fi.WriteString("\n 祥 " + strconv.Itoa(min-i))
			}
			i++
		}

		if count25 > 25 {
			if count4 <= 4 {
				cmd := []string{c, "Get a Break!!!"}
				notify(cmd)
				flag5 = 1
				count4++
			} else {
				cmd := []string{c, "Get a Coffee!!!"}
				notify(cmd)
				flag15 = 1
				count4 = 1
			}

			j := 1
			if flag5 == 1 {
				for j <= 5 {
					_, err := fi.WriteString("\n  " + strconv.Itoa(6-j))
					if err != nil {
						fmt.Println("Error:", err)
					}
					time.Sleep(1 * time.Minute)
					i++
					j++
				}
				flag5 = 0
				j = 1
				cmd := []string{c, "Get back to Work!!!"}
				notify(cmd)

			} else if flag15 == 1 {
				j = 1
				for j <= 15 {
					_, err := fi.WriteString("\n  " + strconv.Itoa(16-j))
					if err != nil {
						fmt.Println("Error:", err)
					}
					time.Sleep(1 * time.Minute)
					i++
					j++
				}
				flag15 = 0
				j = 1
				cmd := []string{c, "Get back to Work!!!"}
				notify(cmd)
			}

			count25 = 1
		}
	}
	go func() { err = os.Truncate(user.HomeDir+"/.pomobaroutput", 0) }()
}
