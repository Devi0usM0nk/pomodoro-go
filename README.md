# Pomodoro-go 
It is a application that applies pomodoro technique and print it to polybar.

## Installation
```shell
$ git clone https://gitlab.com/Devi0usM0nk/pomodoro-go.git
$ cd pomodoro-go
$ cp notify ~/.local/bin/
$ go install
```
Polybar config
